while read PROJECT_ID
do
    SA_NAME="$PROJECT_ID@appspot.gserviceaccount.com"
    echo "Downgrading $SA_NAME"
    gcloud projects add-iam-policy-binding $PROJECT_ID --member "serviceAccount:$SA_NAME" --role roles/logging.logWriter
    gcloud projects add-iam-policy-binding $PROJECT_ID --member "serviceAccount:$SA_NAME" --role roles/monitoring.metricWriter
    gcloud projects add-iam-policy-binding $PROJECT_ID --member "serviceAccount:$SA_NAME" --role roles/monitoring.viewer
    gcloud projects remove-iam-policy-binding $PROJECT_ID --member "serviceAccount:$SA_NAME" --role roles/editor
done < project_ids_to_disable_sa.txt
